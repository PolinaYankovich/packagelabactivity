package secondpackage;
/**
 * @author Polina Yankovich
 * @version 20/08/2022
 */
public class Utilities {
    public static void main(String []args){
/**
 * Double the input value
 */
        System.out.println(doubleMe(10));
    }
    public static int doubleMe(int x){
 /** @param x The value to be doubled
  * @return The value of c times two
  */
        return x*2;
    }
}

